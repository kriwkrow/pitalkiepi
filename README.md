# TalkiePi Client Image

[![pipeline status](https://gitlab.com/kriwkrow/talkiepi-client-image/badges/master/pipeline.svg)](https://gitlab.com/kriwkrow/talkiepi-client-image/-/commits/master)

This repository generates disk image with [TalkiePi](https://github.com/dchote/talkiepi) Mumble client installed. You can download it and use it with your Raspberry Pi without spending config time. The only things you will need is to edit a config file so that your TalkiePi can connect to the server. You should specify a nickname as well.

Software used:

- [TalkiePi](https://github.com/dchote/talkiepi)
- [Mumble](https://www.mumble.info/)
- [Pi-Gen](https://github.com/RPi-Distro/pi-gen)

LICENCE: MIT
