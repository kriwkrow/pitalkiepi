#!/bin/bash -e

GOPATH="/home/mumble/gocode"; export GOPATH
GOBIN="/home/mumble/bin"; export GOBIN

# Enter chroot on Raspberry Pi (act as if you were root on Pi)
on_chroot << EOF
# Test environment
pwd && whoami

# Create mumble user
adduser --disabled-password --disabled-login --gecos "" mumble
usermod -a -G cdrom,audio,video,plugdev,users,dialout,dip,input,gpio mumble

# Switch to mumble user and compile talkiepi
su mumble
mkdir ~/gocode
mkdir ~/bin
cd ${GOPATH}
go get github.com/dchote/gopus
go get github.com/dchote/talkiepi
cd ${GOPATH}/src/github.com/dchote/talkiepi
go build -o /home/mumble/bin/talkiepi cmd/talkiepi/main.go
EOF

# Enter root one more time
on_chroot << EOF
# Set up talkiepi run on boot with our settings
cp /home/mumble/gocode/src/github.com/dchote/talkiepi/conf/systemd/mumble.service /etc/systemd/system/mumble.service
sed -i 's+ExecStart = /home/mumble/bin/talkiepi+ExecStart = /home/mumble/bin/talkiepi -server 192.168.1.105:64738 -username pi+g' /etc/systemd/system/mumble.service
systemctl enable mumble.service

# Set up the right audio interface
sed -i 's/defaults.ctl.card 1/defaults.ctl.card 2/g' /usr/share/alsa/alsa.conf
sed -i 's/defaults.pcm.card 1/defaults.pcm.card 2/g' /usr/share/alsa/alsa.conf
EOF

pwd && ls -al && ls -al work && ls -al deploy

echo "TakliePi Client setup done!"
